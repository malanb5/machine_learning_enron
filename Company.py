#!/usr/bin/python
import pickle
from copy import copy
import numpy as np
from collections import defaultdict
from sklearn.linear_model import LinearRegression
from sklearn.cross_validation import train_test_split
from Plotter import Plotter
import math


class Company:

    # Thereshold value for the proportion of NaN or 0 that will be accepted
    # before the feature is discarded
    MIN_NaN = .45
    MAX_ERROR_PERCENTILE = 90

    def __init__(self, path_name):
        self.feature_set = list()
        self.data_dict = dict()
        self.load_data(path_name)
        self.features_dict = defaultdict(list)
        self.train_test_dict = defaultdict()


    def load_data(self, path_name):
        ### Load the dictionary containing the dataset
        with open(path_name, "r") as data_file:
            self.data_dict = pickle.load(data_file)

        # Extract the feature set
        self.feature_set.append('poi')

        # Removes the email_address feature from the dictionary
        self.helperRemoveFeature('email_address')

        for value in self.data_dict[next(iter(self.data_dict))]:
            if value != 'poi' and value != 'email_address':
                self.feature_set.append(value)

    """ ------------ INITIAL DATA CLEANING ------------"""

    def findNaNProp(self, feature):
        """
        find the proportion of Nan or 0 values of the feature
        :param feature: the feature in question
        :return: the proportion of NaN of the feature
        """
        counter = 0.0
        for key in self.data_dict:
            if self.data_dict[key][feature] == 'NaN' or self.data_dict[key][feature] == 0:
                counter += 1
        return counter / len(self.data_dict)

    def removeObviousOutliers(self):

        remove_names = ["TOTAL", "THE TRAVEL AGENCY IN THE PARK"]
        for key in remove_names:
            self.data_dict.pop(key, 0)

    def helperRemoveFeature(self, feature):

        for key in self.data_dict.keys():
            self.data_dict[key].pop(feature, 0)

    def removeLowDataFeatures(self):
        """
        Removes the features which have less than the MIN_NaN allowable
        in the dataset
        """

        for feature in copy(self.feature_set):
            min_for_feature = self.findNaNProp(feature)
            if min_for_feature > Company.MIN_NaN and not feature == 'poi':
                self.feature_set.remove(feature)
                self.helperRemoveFeature(feature)

    def removeLowDataEmployee(self):

        copy_dict = copy(self.data_dict)

        tot_features = len(self.feature_set)

        for key in copy_dict:
            counter = 0
            for feature in self.feature_set:
                if(copy_dict[key][feature] == 'NaN'):
                    counter += 1
            if(float(counter) / float(tot_features) > Company.MIN_NaN):
                del self.data_dict[key]


    """ ------------ OUTLIER CLEANING ------------"""
    def outlierCleaner(self):
        """
        cleans outliers -- this is done by pairwise taking a linear
        regression between two features and eliminating the top percentile
        (MAX_ERROR_PERCENTILE) of the squared deviation between the linear
        regression and the expected feature value
        """
        counter = 0
        past = ''

        for feature in self.feature_set:
            if feature == 'poi':
                pass
            elif counter % 2 == 1:
                past_in = self.reshapeNumpy(past)
                feature_in = self.reshapeNumpy(feature)

                # Plots the two features pairwise
                # uncomment below to see before and after
                # self.plot(past, feature)

                self.calcReg(past, feature, past_in, feature_in)
                # resets the dictionary of the features and repopulates it
                # based upon the removed outliers
                self.resetFeatureDict()

                # uncomment below to see before and after
                # self.plot(past, feature)

                counter += 1

            else:
                past = feature
                counter += 1

    def calcReg(self, feature, past, past_in, feature_in):
        """
        calculates the linear regression between the two features
        and also calls upon a helper function findError which
        gives the points to remove based on their deviance from
        the line of best fit
        """
        reg = LinearRegression()
        reg.fit(past_in, feature_in)

        # print(reg.score(past_in, feature_in))
        predictions = reg.predict(past_in)
        to_remove = self.findError(predictions, feature_in, past_in)

        self.findDelete(feature, past, to_remove)


    def findError(self, predictions, feature_in, past_in):
        """
        finds a given percentile of the most error in the
        line of best fit and the expected value (difference squared)

        returns a list of tuples of the points to remove based upon
        that percentile value
        """

        error = ((feature_in - predictions) ** 2)

        error_tuple = zip(error, past_in, feature_in)

        markPercentile =np.percentile(error, Company.MAX_ERROR_PERCENTILE)

        to_remove = []

        for each in error_tuple:
            if(each[0] > markPercentile):
                to_remove.append(each)

        return to_remove

    """ ------------ FEATURE CREATION------------"""
    def createFeature(self, name, numer, divisor):
        """
        creates a new feature for the main data dictionary
        """

        for key in copy(self.data_dict):
            employee = self.data_dict[key]
            if employee[numer] != 'NaN' and employee[divisor] != 'NaN':
                employee[name] = float(employee[numer]) / float(math.log((employee[divisor]), 2))
            else:
                employee[name] = 'NaN'

        self.feature_set.append(name)


    """ ------------ UTILITY FUNCTIONS ------------"""

    def findDelete(self, feature, past, to_remove):
        """
        deletes a specific key, value pair from the main dictionary based on a specific criterion
        """

        for val in to_remove:
            value, past_value = val[1], val[2]

            for key in copy(self.data_dict):
                if self.data_dict[key][feature] == value[0] and self.data_dict[key][past] == past_value[0]:
                    self.data_dict.pop(key, 0)

    def plot(self, past, feature):
        # Plots the two features pairwise
        plotter = Plotter(self.features_dict)
        plotter.plotScatter(past, feature)

    def reshapeData(self):
        """
        creates a dictionary of features with numerical data attached to each
        """
        for feature in self.feature_set:
            for key in self.data_dict:
                if(self.data_dict[key][feature] == 'NaN'):
                    self.features_dict[feature].append(0)
                else:
                    self.features_dict[feature].append(self.data_dict[key][feature])

    def reshapeNumpy(self, feature):
        return np.reshape(np.array(self.features_dict[feature]), (len(self.features_dict[feature]), 1))

    def getDict(self):
        # Returns a copy of the dictionary of company data
        return copy(self.data_dict)

    def getFeatures(self):
        # Returns a copy of the features list
        return copy(self.feature_set)

    def query(self, feature, limit):
        results = list()
        for key in self.data_dict:
            if (self.data_dict[key][feature] < limit and self.data_dict[key][feature] != 'NaN'):
                results.append(key)
        return results

    def lookup(self, key):

        return self.data_dict.get(key)

    def resetFeatureDict(self):
        self.features_dict = defaultdict(list)
        self.reshapeData()