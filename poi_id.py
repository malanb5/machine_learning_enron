#!/usr/bin/python
"""
poi_id the main script which creates a Company object which acts
as a data and utility class for the company data which is stored
as a dictionary of dictionaries.

Also makes use of a Plotter class to visualize the data
"""
import sys
import pickle
from sklearn import *
from pandas import pandas as pd
from Company import Company
from collections import defaultdict
from sklearn.preprocessing import MinMaxScaler


sys.path.append("../tools/")

from feature_format import featureFormat, targetFeatureSplit
from tester import test_classifier, dump_classifier_and_data

## create an object which will hold the company data
company = Company("final_project_dataset.pkl")
### Extract features and labels from dataset for local testing

# Removes features from the dataset which have a certain threshold
# of NaN's and empty fields
company.removeLowDataFeatures()
company.removeLowDataEmployee()

# Remove bad names and outliers from the data
company.removeObviousOutliers()

# Reshapes the numeric data into a dictionary of lists
company.reshapeData()

### Task 2: Remove outliers
company.outlierCleaner()

### Remove features which have low F1 score and little affect on our model
# to_remove = []
to_remove = ['to_messages', 'from_messages', 'other']

for feature in to_remove:
    company.helperRemoveFeature(feature)
    company.feature_set.remove(feature)


### Task 3: Create new feature(s)
company.createFeature('ratio_exercised_to_total', 'exercised_stock_options', 'total_stock_value')

### Store to my_dataset for easy export below.
data_dict = company.getDict()

my_dataset = data_dict
features_list= company.getFeatures()

data = featureFormat(my_dataset, features_list, sort_keys = True)

labels, features = targetFeatureSplit(data)

### Scale features
scaler = MinMaxScaler()
features = scaler.fit_transform(features)

### Task 4: Try a varity of classifiers
### Please name your classifier clf for easy export below.
### Note that if you want to do PCA or other multi-stage operations,
### you'll need to use Pipelines. For more info:
### http://scikit-learn.org/stable/modules/pipeline.html

from sklearn.cross_validation import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score, precision_score, recall_score
from sklearn import tree
from sklearn.preprocessing import StandardScaler

# Provided to give you a starting point. Try a variety of classifiers.
class_option = 0
clf = GaussianNB()

if class_option == 1:
    clf = DecisionTreeClassifier(min_samples_split=5)

elif class_option == 2:
    clf = KNeighborsClassifier(n_neighbors=1, algorithm='ball_tree', leaf_size = 10)

elif class_option == 3:
    clf = RandomForestClassifier(n_estimators= 4)

### Task 5: Tune your classifier to achieve better than .3 precision and recall 
### using our testing script. Check the tester.py script in the final project
### folder for details on the evaluation method, especially the test_classifier
### function. Because of the small size of the dataset, the script uses
### stratified shuffle split cross validation. For more info: 
### http://scikit-learn.org/stable/modules/generated/sklearn.cross_validation.StratifiedShuffleSplit.html

def tune(feature_list, dataset):

    knn = KNeighborsClassifier()
    # feature scale
    estimators = [('scale', StandardScaler()), ('knn', knn)]
    pipeline = Pipeline(estimators)
    parameters = {'knn__leaf_size': [1, 10],
                  'knn__algorithm': ('ball_tree', 'kd_tree')}
    clf = GridSearchCV(pipeline, parameters, scoring='recall')
    test_classifier(clf, my_dataset, features_list)

    ## Best param
    print clf.best_params_

### tune the classifier, GaussianNB was the best classifier even with tuning
# tune(features_list, my_dataset)

test_classifier(clf, my_dataset, features_list)

### Task 6: Dump your classifier, dataset, and features_list so anyone can
### check your results. You do not need to change anything below, but make sure
### that the version of poi_id.py that you submit can be run on its own and
### generates the necessary .pkl files for validating your results.

dump_classifier_and_data(clf, my_dataset, features_list)