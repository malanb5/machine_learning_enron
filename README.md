## Enron Machine Learning Project
Matt Bladek
6/29/2018

### Summary
The goal of the project was to come up with a classifier to see if features in a series of emails and financial data on employees at Enron indicated that they were involved in corruption or not. The label in this case would be if they were a “person of interest” or “poi”, which was someone would was indicted, reached a settlement or plea deal with the government, or testified in exchange for prosecution immunity.

The dataset prepared on the emails was a dictionary of each Enron employee with features of the employee including their financial characteristics as well as their email characteristics also whether or not they were a person of interest or not.

### Class Design
Company class is the data and utility class which does the heavy lifting of this analysis. It contains the dictionary of dictionary of employees and features, the feature list, and dictionary of numpy arrays of the features. It removes outliers, creates features, and has a number of utility type functions.

Plotter class utilizes ggplot to plot and converts the features array into a pandas dataframe.

### Data Exploration (before cleaning/outlier removal)
Total Number of Employees:
146

Number of Features:
20

List of Features:
‘poi’, ‘salary’, ‘to_messages’, ‘deferral_payments’, ‘total_payments’, ‘exercised_stock_options’, ‘bonus’, ‘restricted_stock’, ‘shared_receipt_with_poi’, ‘restricted_stock_deferred’, ‘total_stock_value’, ‘expenses’, ‘loan_advances’, ‘from_messages’, ‘other’, ‘from_this_person_to_poi’, ‘director_fees’, ‘deferred_income’, ‘long_term_incentive’, ‘from_poi_to_this_person’

Allocation Across Classes
Number of POI = 18 Number of Non-POI = 128

### Data Cleaning and Outliers
The data was incomplete and there were a few employees which were not employees. I first eliminated data points which were obviously not employees such as “TOTAL”.

Missing values were throughout the data and therefore features and employees that did not meet a minimum threshold of .45 of the data being intact were eliminated from the dataset.

The remaining features were then fit with linear regression and the square of the error from the line of best fit to the actual point was taken to judge. The top 10% of the data which demonstrated the most deviance from the line of best fit was then eliminated from the data. In the company class, function outlierCleaner there is two commented out lines by which one could view the before and after of the result of cleaning the 10% of the outliers.

### Feature Selection
Features were first dropped based upon their lack of data. Several different thresholds were tested for which NaN was dropped at. At 30% of the values being only 4 features remained and with a GaussianNB model, precision was at .346 and recall .076. At 40% the precision dropped to .059 and reall .026.
At 50% the precision was .574 and the recall .512. At 60% the precision was .329 and the recall was .471. At 45%, preicison was .4956 and recall .676 with an F1 score of .57191.

Therefore, 45% was determined to be the local maximum and therefore is the threshold of the cut off for which a feature or employee would be dropped from the dataset if the ratio exceed this threshold of NaN’s to total observations.

Features were then selected based upon taking their F1 score and using a GaussianNB model and determining which would be the most useful in classification and when sorted the lowest features were ‘to_messages’ had 0 precision and recall, ‘from_messages’ had 0 precision and recall. ‘other’ had .29 precision and .059 recall. ‘from_messages’ and ‘other’ had low F1 score and therefore was eliminated from the model.

The features which were chosen for the model were: [‘poi’, ‘salary’, ‘total_payments’, ‘exercised_stock_options’, ‘bonus’, ‘restricted_stock’, ‘shared_receipt_with_poi’, ‘total_stock_value’, ‘expenses’, ‘ratio_exercised_to_total’]

A feature was created which took the ratio of the two highest deterministic features –total_stock_value and exercised_stock_options. The log transformation of the total_stock_value was taken. This improved the F1 score from .45 to .57 with this new feature.

After much numerous testing, the model which provided us the best results was the GaussianNB therefore our features did not need to scaled. However, the max min scaling was done for other algorithms as required.

### Algorithm
GaussianNB gave us the highest F1 score of .57191. An accuracy of .85543, Precision of .4956, and Recall: .67600.

KNieghbors, Decision Tree, and Random Forest Classifiers were all tried and tuned but could not beat the performance of the GaussianNB.

### Tuning
Tuning parameters is important in that the algorithm behaves differently based upon said parameters. Any change to the algorithm and how it functions will give either a better or worse fit to the data. Therefore it is important that models that have many parameters are tuned to ensure the model is as precise and accurate as possible.

GridSearchCV from the sklearn toolkit was used to tune algorithms with multiple parameters.
K-Neighbors Classifier was tuned with three parameters: n_neighbors, algorithm, and leaf_size. The number of neighbors parameter N had a default setting of 5. Through tuning the optimal number was 1. Tuning the best algorithm was ball-tree. The leaf_size default of 30, it was abandoned in favor of a smaller leaf size of 1. The tuned K-Neighbors Classifier had parameters of: n_neighbors of 1, algorithm of ‘ball_tree’ and leaf_size of 1.

Although the traditional GaussianNB outperformed all other algorithms and did not need any tuning.

### Validation
Validation is checking or proving the accuracy of a model. This is important in any endeavor to ensure that you are building something which does what it says it will do.

For our model, two methods were employed to validate the model. The first was using the method from the tester module–test_classifier. This method, split the data into two sets of data a training set and testing set using the StratifiedShuffleSplit from skearn’s cross validation module. Fitting the model using the training data, using the testing data to make preditions and validating them with the corresponding labels from the testing data.

GridSearchCV was also used to tune parameters and it had it’s own validation method to find the best parameters. However, each model was validated with the method of test_calssifier, which is outlined above as to how it works.

Using this cross validation method: GaussianNB accuracy was .8554 which meant the classifier correctly predicted 85.54% of the POI from the total number of employees.
The precision was .4956 which meant that out of the predicted POI about half were actual positive. The recall was .676 which means that of all of the actual positive cases, the classifier correctly predicted 67.6% of them.

### Limitations and Extensions
There are many extensions and things that could be done to improve this model and study. First the classifier was of who was caught committing fraud and publically labeled as a fraudster. However this classification in and of itself is also subject to error. Someone could have committed fraud but not gone after by the Justice Department or likewise someone could have been implicated who was actually innocent.

I would argue that this model is very localized to this one incident as most companies do not have such high amount of fraud and in such a high dollar amount. Therefore, if one were to apply this to another company many false positives would arise.

An extension would be to see if model held with another company wrought with fraud.

### Conclusion
Can we predict whether someone at Enron would be implicated in Fraud? Not very accurately. Through this project the data was cleaned, outliers were removed, the top predictive features were taken, model was trained, tested, tuned, and validated. The Gaussian Naive Bayes model was the most effective classification algorithm based on all performance indicators.
